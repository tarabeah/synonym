<?PHP

namespace model\main;

Class user extends \Icore\Synonym\BasicModel {

    static protected $columns = [
        'id' => 'int',
        'country_id' => 'int',
        'type' => array('admin', 'author'),
        'name' => 'string'
    ];

    static protected $relations = [
        'country' => ['one-to-one' => ['country_id' => 'country.id']], // many-to-one
        'comment' => ['one-to-many' => ['id' => 'comment.user_id']],
        'books' => ['many-to-many' => ['books.id' => 'users_books.book_id', 'users_books.user_id' => 'user.id']]
    ];

}
