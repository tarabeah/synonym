<?PHP

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '../vendor/autoload.php';
include_once 'menu.php';

# https://www.mysqltutorial.org/mysql-adjacency-list-tree/

$con = new \Icore\Queryforge\DB();
$con->define('main', 'mysql', array("mysql:host=127.0.0.1;dbname=tree", 'root', ''));

$menuObj = new model\mysql\menu();
$menuObj->setConnectionName('main');


// $data = $menuObj->getTree();
// echo '<xmp>';
// print_r($data);
// exit;


// $insert = $menuObj->insert(2, ['title' => 'Hellossss'], 1);


// $menuObj->delete(2);

$data = [
    'id' => 4,
    'title' => 'PC"\'"s\'',
    'parent_id' => '5',
    'sorting' => 1,
];
$menuObj->set($data);

