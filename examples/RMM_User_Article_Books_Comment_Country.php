<?php

Class RMM_User_Article_Books_Comment_Country extends Icore\Synonym\RelationModel{

    protected static $tables = array('main' => 'user',  'comment', 'article', 'country', 'books');
    protected static $relation = [
        ['comment.article_id' => 'article.id'],
        [['books.id' => 'users_books.book_id'], ['users_books.user_id' => 'user.id']],
        ['article.user_id' => 'user.id'],
        ['country.id' => 'user.country_id'],
    ];

    function whereCustom(){
       // $this->query()->where('article.id', '5', '=');
        return $this;
    }
}

