<?PHP

namespace model\main;

Class comment extends \Icore\Synonym\BasicModel {

    static protected $columns = array(
        'id' => 'int',
        'article_id' => 'int',
        'user_id' => 'int',
        'content' => 'string',
        //'active' => 'bit'
    );

}
