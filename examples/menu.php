<?PHP

namespace model\mysql;

CLASS menu extends \Icore\Synonym\TreeModel  {

    static protected $columns = array(
        'id' => 'int',
        'parent_id' => 'int',
        'sorting' => 'int',
        'title' => 'string',
    );
}
