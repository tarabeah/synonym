<?PHP

namespace model\main;

Class article extends \Icore\Synonym\BasicModel {

    static protected $columns = array(
        'id' => 'int',
        'user_id' => 'int',
        'published_at' => 'string',
        'title' => 'string',
        'content' => 'string'
    );

}
