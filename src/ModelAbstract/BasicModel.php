<?PHP namespace Icore\Synonym\ModelAbstract;

use PHPUnit\Runner\Exception;

abstract class BasicModel extends ModelAbstract
{
    static protected $columns;
    protected $data;
    protected $newData;
    protected $id = null;
    protected static $primaryId = 'id';
    protected $CustomQueryBeforeExecuteToGet = null;
    protected $isThisCollection = false;
    protected $waitingForOneToOneCall = false;
    protected $BaseQueryReplacement = null;

    /**
     * Set Data or update its old value for this entity.
     *
     * @param $columnName
     * @param $value
     * @throws \Exception
     */
    public function data()
    {
        $args = func_get_args();

        if (is_array($args[0])) {
            // if the first argument is array that mean we would like to set multi columns at once.
            $this->updateSetOfData($args[0]);
        } else if (func_num_args() == 2) {
            // if there are 2 argument and the first one is String, then we expecting that this request
            // is asking for update or set one column.
            $this->setEntityData($args[0], $args[1]);
        } else {
            // otherwise this request in not covered.
            throw new \Exception('Invalid arguments');
        }
    }

    /**
     * Delete row from table by id
     *
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    protected function directDeleteById($id)
    {
        if (is_array($id)) {
            $this->BaseQuery->deleteFrom($this->table)->where(static::$primaryId, $id, 'in');
        } else if (is_int($id)) {
            $this->BaseQuery->deleteFrom($this->table)->where(static::$primaryId, $id, '=');
        } else {
            throw new \Exception('Invalid deleted id (' . __LINE__ . ')');
        }

        try {
            $result = $this->BaseQuery->exec();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage() . ' (' . __LINE__ . ')');
        }

        return $result;
    }

    /**
     * @param $dataArray
     * @throws \Exception
     */
    protected function updateSetOfData($dataArray)
    {
        foreach ($dataArray as $columnName => $value) {
            $this->setEntityData($columnName, $value);
        }
    }

    /**
     * Set value for a column in order to save or update it later
     *
     * @param $columnName
     * @param $value
     * @throws \Exception
     */
    public function setEntityData($columnName, $value)
    {
        if (!array_key_exists($columnName, static::$columns)) {
            throw new \Exception('There is no ' . $columnName . ' column in ' . get_class($this) . ' table.');
        }

        $this->newData[$columnName] = $value;
    }

    /**
     * Initialize Base query from QueryForge class.
     * The base query used to build target query, and it is common object that could be
     */
    public function initQuery()
    {
        // Clear Fluent object by creating new Queryforge\Query
        $this->BaseQuery = new \Icore\Queryforge\Query($this->connectionName);

        // Prepare variable to handle the the sequence of customization for query before call get() or all().
        $this->CustomQueryBeforeExecuteToGet = new \Icore\Synonym\CustomQueryBeforeExecuteToGet();
    }

    /**
     * Return the data as array.
     *
     * @return mixed
     */
    public function result()
    {
        $return = array();
        if ($this->data) {
            // if this is collection - called as list - there is no need to add
            // the id, since the list of objects will be returned:
            if (!$this->isThisCollection) {
                $return = [static::$primaryId => $this->id];
            }
            $return = array_merge($return, $this->data);
        }
        return $return;
    }

    /** Set the list of Models as collection in one object from Util\ModelCollection type.
     *
     * @param $data
     * @return mixed
     */
    public function collection($data)
    {
        $collection = new \Icore\Synonym\Util\ModelCollection($data, get_class($this));
        return $collection->getCollection($this->connectionName);
    }

    /**
     * When we use the collection, we need to set the values for each Model in the list
     * so the attribute for the model should be updated.
     *
     * @param $attrs
     * @throws \Exception
     */
    public function setAttributes($attrs)
    {
        // check if the object is empty or not, since the concert object should not be updated
        // by this function.
        if ($this->id !== null) {
            throw new \Exception('The ' . get_class($this) . ' is already declared for another object.');
        }

        // set attributes values.
        foreach ($attrs as $attName => $attValue) {
            $this->$attName = $attValue;
        }
    }

    // return the static name
    static function getPrimaryId()
    {
        return static::$primaryId;
    }

    /**
     * Return columns names in order to embed them in SQL query
     */
    protected function clms()
    {
        return '`' . implode('`, `', array_keys(static::$columns)) . '`';
    }

    public static function getColumnsAttr()
    {
        return static::$columns;
    }

    /**
     * Return the querystring that executed in SQL
     *
     * @return string
     */
    public function queryString()
    {
        return $this->queryString;
    }


    /**
     * @param $modelName
     * @return mixed
     */
    public function bind($modelName)
    {

        $relationType = key(static::$relations[$modelName]);
        switch ($relationType) {
            case 'one-to-one':
            case 'many-to-one':
                return $this->getOneToOneModel($modelName, $relationType, true);
                break;
            case 'one-to-many':
                return $this->getOneToOneModel($modelName, $relationType, false);
                break;
            case 'many-to-many':
                return $this->getManyToManyModel($modelName);
                break;
        }
    }

    /**
     *
     * @return mixed
     */
    public function orm()
    {
        $return = $this->list();
        return $return;
    }

    /**
     * Return ORM object while the relation is 1 to 1
     *
     * @param $modelName
     * @param bool $waitingForOneToOneCall
     * @return mixed
     */
    private function getOneToOneModel($modelName, $relationType, bool $waitingForOneToOneCall = true)
    {
        $relationTableName = strstr(array_values(static::$relations[$modelName][$relationType])[0], '.', true);
        $relationTableKey = $relationTableName . strstr(array_values(static::$relations[$modelName][$relationType])[0], '.');
        $relationTableKeyValue = $this->data[key(static::$relations[$modelName][$relationType])];

        $modelClassName = $modelClassName = '\model\\' . $this->connectionName . '\\' . $relationTableName;

        if (!class_exists($modelClassName)) {
            include_once __SITE_PATH . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . $this->connectionName . DIRECTORY_SEPARATOR . $relationTableName . '.php';
        }

        $modelClassName = new $modelClassName();
        $modelClassName->setConnectionName($this->connectionName);
        $modelClassName->addWhere($relationTableKey, $relationTableKeyValue);

        if ($waitingForOneToOneCall) {
            $modelClassName->waitingForOneToOneCall();
        }
        return $modelClassName;
    }

    /**
     * Bind one-to-many OR many-to-one relation for models
     *
     * @param $modelName
     */
    private function getManyToManyModel($modelName)
    {
        $relations = static::$relations[$modelName]['many-to-many'];

        // Get all keys that requested for relation
        $tblsKey[] = key($relations);
        $tblsKey[] = $relations[key($relations)];
        unset($relations[key($relations)]);
        $tblsKey[] = key($relations);
        $tblsKey[] = $relations[key($relations)];

        // extract tables name from keys:
        $tables = [];
        foreach ($tblsKey as $key) {
            $tables[] = strstr($key, '.', true);
        }

        // Handel the "mani join" && "target" tables name:
        $tablesCount = array_count_values($tables);
        $mainJoinTable = array_flip($tablesCount)[2];
        unset($tablesCount[$mainJoinTable]);
        unset($tablesCount[$this->table]);
        $targetTable = key($tablesCount);

        // Get target model class:
        $modelClassName = $modelClassName = '\model\\' . $this->connectionName . '\\' . $targetTable;

        // then load it if it is not exist:
        if (!class_exists($modelClassName)) {
            include_once __SITE_PATH . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . $this->connectionName . DIRECTORY_SEPARATOR . $targetTable . '.php';
        }

        // Set select columns:
        $selectColumns = [];
        foreach ($modelClassName::getColumnsAttr() as $columnsName => $type) {
            $selectColumns[] = $targetTable . '.' . $columnsName;
        }

        // extract "on" and FK name:
        $relations = static::$relations[$modelName]['many-to-many'];
        foreach ($relations as $relationLeft => $relationRight) {
            $leftTable = strstr($relationLeft, '.', true);
            $rightTable = strstr($relationRight, '.', true);

            if ($leftTable != $this->table && $rightTable != $this->table) {
                if(isset($on)){
                    throw new \Exception('Invalid many to many relation, please check tables name.');
                }
                $on = [$relationLeft, $relationRight];
            } else {
                if($leftTable == $this->table){
                    $where = $relationRight;
                    $FK = strstr($relationLeft, '.', true);
                } else {
                    $where = $relationLeft;
                    $FK = strstr($relationRight, '.', false);
                    $FK = trim($FK, '.');
                }
            }
        }

        // Build query:
        $query = new \Icore\Queryforge\Query($this->connectionName);
        $query->from($targetTable)
            ->innerJoin($mainJoinTable)->on($on[0], $on[1], '=')
            ->where($where, $this->data[$FK], '=')
            ->select(null);
        call_user_func_array(array($query, 'select'), $selectColumns);

        $modelObj = new $modelClassName();
        $modelObj->setConnectionName($this->connectionName);
        $modelObj->setQuery($query);
        $modelObj->list();

        return $modelObj;
    }

    /**
     * Enable waitingForOneToOneCall, that mean return first item from the collection in $this->list()
     */
    private function waitingForOneToOneCall()
    {
        $this->waitingForOneToOneCall = true;
    }

    public function setQuery($query){
        $this->BaseQueryReplacement = $query;
    }

    function limit($start, $end){
        $this->query()->limit($start, $end);
        return $this;
    }

    function count(){
        return $this->aggregate(['COUNT(*) as `count`']);
    }
}
