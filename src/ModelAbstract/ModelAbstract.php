<?PHP namespace Icore\Synonym\ModelAbstract;

abstract Class ModelAbstract {

    protected $connectionName = '';
    protected $BaseQuery = '';
    protected $queryString = '';
    protected $table = '';
    protected $queryCustom = null;

    abstract public function data();

    /**
     * This method used to handle connections by name
     *
     * @param $connectionName
     * @param $conn
     */
    public function setConnectionName($connectionName)
    {
        $this->connectionName = $connectionName;

        $classSegments = explode("\\", get_class($this));
        $this->table = end($classSegments);

        $this->initQuery();
    }


    /**
     * Build the query and after adding the customer parts set the Query string and it's parameters.
     *
     * @return mixed
     */
    public function exec()
    {
        // Prepare the Select where:
        // Note: the where is added to make any custom parts of query added by using (and) or (or) directly.
        $this->addCustomQueryPartsToBaseQuery();

        // execute the QueryForge request
        $return = $this->BaseQuery->exec();

        // Update the QueryString to make it traceable for this model
        $this->queryString = \Icore\Queryforge\Query::getLastQuery();

        // Reset $query before return the data.
        $this->initQuery();

        return $return;
    }

    /**
     * This method use to handle additional customization to the get() before execute it
     */
    protected function addCustomQueryPartsToBaseQuery()
    {
        if ($this->queryCustom !== null) {
            $this->queryCustom->buildQeury($this->BaseQuery);
        }
    }

    /**
     * Return the object that will use to handle the customization part of select query
     * in order to concatenate it with Fluent object when build it.
     *
     * @return mixed
     */
    protected function query()
    {
         return $this->queryCustom = is_null($this->queryCustom) ? new \Icore\Synonym\CustomQueryBeforeExecuteToGet() : $this->queryCustom;
    }

    public function setQueryString($queryCustom){
        $this->queryString = $queryCustom;
    }
}
