<?PHP namespace Icore\Synonym;


abstract class TreeModel extends \Icore\Synonym\BasicModel {

    private $treeName = '';
    static protected $columns;
    private $errors = [];
    private $where = [];
    private $limit = '';

    /**
     * Select tree from database and return the records
     *
     * @param null $parent_id
     * @return array
     */
    public function getTree($parent_id = null){
        $query = $this->buildQuery($parent_id);
        $tree = $this->runQuery($query);

        if($this->limit){
            $return = [];
            foreach($tree as $item){
                $pathOfSortingInTree = $item['path_of_sotring_in_tree'];
                $pathOfSortingInTree = explode('>', $pathOfSortingInTree);
                $depth = count($pathOfSortingInTree);
                unset($item['path_of_sotring_in_tree']);
                $item['_depth'] = $depth;
                $return[] = $item;
            }

            $this->limit = '';
            return $return;
        } else {
            return $dataHierarchical = $this->childes($tree, $parent_id);
        }
    }

    /**
     * insert new item in tree
     *
     * @param $parent_id
     * @param $data
     * @param null $sorting
     * @return mixed
     * @throws \Exception
     */
    public function add($parent_id, $data, $sorting=null){

        \Icore\Queryforge\DB::transStart();
        try {
            $getExistSoringForThisParent = $this->soringByParent($parent_id);

            if(in_array($sorting, $getExistSoringForThisParent)){
                if(!$this->shif($parent_id, $sorting)){
                    throw new \Exception('Cont shif the sorting');
                }
            }

            if($sorting === null){
                $sorting = $getExistSoringForThisParent ? $getExistSoringForThisParent[count($getExistSoringForThisParent) - 1] + 1: 1;
            }

            $data = array_merge($data, ['sorting' => $sorting, 'parent_id' => $parent_id]);

            $values = array_values($data);
            $keys = implode(', ' , array_keys($data));

            // Build ? parameter in the query
            ${'?'} = array_fill(0, count($data), '?');
            ${'?'} = implode(',', ${'?'});

            //$sql = 'INSERT INTO ' . $this->table . ' (' . $keys . ') VALUES (' . ${'?'} . ')';
            $sql = 'INSERT INTO ' . $this->table . ' (' . $keys . ') VALUES (' . ${'?'} . ')';
            $fluentPdo = \Icore\Queryforge\DB::getPDO($this->connectionName);
            $fluentPdo->prepare($sql)->execute($values);

            $this->arrangeSortingSequence($parent_id);

        } catch (\Exception $e){
            \Icore\Queryforge\DB::rollBack();
            $this->errors = $e->getMessage();
            return false;
        }

        \Icore\Queryforge\DB::commit();

        return $fluentPdo->lastInsertId();
    }

    /**
     * Update data.
     *
     * @param $data
     * @return false
     * @throws \Exception
     */
    public function set($data){
        $id = $data['id'];
        $originData = $this->get($id)->result();

        // this item is not exist, so return false and close the process
        if(!$originData){
            return false;
        }

        if(!isset($data['parent_id'])){
            $data['parent_id'] = $originData['parent_id'];
        }

        \Icore\Queryforge\DB::transStart();

        try {
            // if there is no sorting value, that mean we will add it in the end of new parent childs
            if (!isset($data['sorting']) && $data['parent_id'] != $originData['parent_id']) {
                $getExistSoringForThisParent = $this->soringByParent($data['parent_id']);
                $data['sorting'] = $getExistSoringForThisParent ? $getExistSoringForThisParent[count($getExistSoringForThisParent) - 1] + 1 : 1;
            } elseif (isset($data['sorting']) && $data['parent_id'] != $originData['parent_id']) {
                // in this case we would like to move from parent to another parent with specific sorting, so
                // if this sorting is already reserved then we have shift child for new item:
                $getExistSoringForThisParent = $this->soringByParent($data['parent_id']);
                if (in_array($data['sorting'], $getExistSoringForThisParent)) {
                    $this->shif($data['parent_id'], $data['sorting']);
                }
            } else {
                // this area is reached when the $data['parent_id'] == $originData['parent_id']
                if (isset($data['sorting'])) {
                    if ($data['sorting'] != $originData['sorting']) {
                        // there is reevet position in sorting, so shift before the new position
                        $this->shif($data['parent_id'], $data['sorting']);
                    }
                } else {
                    // there is no sorting, sit it in the end of sublist
                    $getExistSoringForThisParent = $this->soringByParent($data['parent_id']);
                    $data['sorting'] = $getExistSoringForThisParent ? $getExistSoringForThisParent[count($getExistSoringForThisParent) - 1] + 1 : 1;
                }
            }

            // Build save query
            $sql = 'Update ' . $this->table . ' SET ';
            unset($data['id']);

            $updatePart = [];
            foreach(array_keys($data) as $key){
                $updatePart[] = $key . ' = ?';
            }

            $updatePart = implode(', ', $updatePart);
            $sql .= $updatePart;
            $sql .= ' WHERE id =' . $id;

            $fluentPdo = \Icore\Queryforge\DB::getPDO($this->connectionName);
            $fluentPdo->prepare($sql)->execute(array_values($data));


            $this->arrangeSortingSequence($data['parent_id']);

            if($originData['parent_id'] != $data['parent_id']) {
                $this->arrangeSortingSequence($originData['parent_id']);
            }

        } catch (\Exception $e){
            \Icore\Queryforge\DB::rollBack();
            throw new \Exception($e->getMessage());
        }

        \Icore\Queryforge\DB::commit();

    }

    /**
     * Delete item and its childes by updating "Delete" flag
     *
     * @param null $id
     * @return bool|TreeModel|null
     * @throws \Exception
     */
    public function delete($id = null){
        $allChildes = $this->getChiledesByParentId($id);

        $childIds = implode(',', $allChildes);
        $sql = 'UPDATE ' . $this->table . ' SET deleted=1 WHERE id IN (' . $childIds . ')';

        $fluentPdo = \Icore\Queryforge\DB::getPDO($this->connectionName);

        try {
            $fluentPdo->prepare($sql)->execute();
        } catch (\Exception $e){
            $this->errors[] = $e->getMessage();
            return false;
        }

        return true;
    }

    /**
     * Delete item with its child totally from data base
     * @param null $id
     * @return bool
     */
    public function remove($id = null){
        $allChildes = $this->getChiledesByParentId($id);

        $childIds = implode(',', $allChildes);
        $sql = 'DELETE FROM ' . $this->table . ' WHERE id IN (' . $childIds . ')';

        $fluentPdo = \Icore\Queryforge\DB::getPDO($this->connectionName);

        try {
            $fluentPdo->prepare($sql)->execute();
        } catch (\Exception $e){
            $this->errors[] = $e->getMessage();
            return false;
        }

        return true;
    }

    /**
     * if there is more than one tree in the related table in the database, then
     * we classified them by tree_name column, since there is specific name for each one
     * for example if this is menus tree, then we may have top_menu, left_menu, bottom_menu .. etc
     * and every elements for each menu have its name in tree_name column
     *
     * @param $treeName
     */
    public function setTreeName($treeName){
        $this->treeName = $treeName;
    }

    /**
     * @param int $status
     * @return $this|TreeModel
     */
    public function active($status = 1)
    {
        $this->where[] = 'active = ' . $status;
        return $this;
    }

    /**
     * @param int $deleted
     * @return $this
     */
    public function deleted($deleted = 1)
    {
        $this->where[] = 'deleted = ' . $deleted;
        return $this;
    }

    /**
     * Return sub tree by limitation:
     *
     * @param $offset
     * @param $length
     * @return TreeModel|void
     */
    public function limit($offset, $length){
        $this->limit = ' LIMIT ' . $offset . ', ' . $length;
        return $this;
    }


    /**
     * Select parent and its childes.
     *
     * @param $parent_id
     * @return string
     */
    private function buildQuery($parent_id){

        $columns = implode(', ', $this->getColumnsName());
        ${'c.columns'} = implode(', ', $this->getColumnsName('c'));

        if($parent_id === null){
            $whereParentId = 'WHERE parent_id IS NULL';
        } else {
            $whereParentId = 'WHERE id = ' . $parent_id;
        }
        $query = '
                    WITH RECURSIVE category_path (' . $columns . ', path_of_sotring_in_tree) AS
                    (
                      SELECT ' . $columns . ', CAST(sorting as CHAR) as path_of_sotring_in_tree
                        FROM ' . $this->table . '
                        ' . $whereParentId . '
                      UNION ALL
                      SELECT ' . ${'c.columns'} . ', CONCAT(cp.path_of_sotring_in_tree, \'>\', CAST(c.sorting as CHAR))
                        FROM category_path AS cp JOIN ' . $this->table . ' AS c
                          ON cp.id = c.parent_id
                    )
                    SELECT * FROM category_path
        ';

        if($this->treeName){
            $this->where[] = ' tree_name = "' . $this->treeName . '" ';
        }

        if($this->where){
            $where = implode(' AND ', $this->where);
            $query = $query  . ' WHERE ' . $where;
        }

        $query .='
                        ORDER BY path_of_sotring_in_tree
                    ';

        if($this->limit){
            $query .= $this->limit;
        }

        return $query;
    }

    /**
     * Run fetch query
     *
     * @param $query
     * @return mixed
     */
    private function runQuery($query){
        $fluentPdo = \Icore\Queryforge\DB::getPDO($this->connectionName);
        $stmt = $fluentPdo->query($query);
        $tree = $stmt->fetchall(\PDO::FETCH_ASSOC);

        return $tree;
    }

    /**
     * Return the lement id and all it is chiled
     * @param $parent_id
     * @return array
     */
    private function getChiledesByParentId($parent_id){
        $query = $this->buildQuery($parent_id);
        $tree = $this->runQuery($query);

        $childIds = [];

        // Get id's for all childes their parent id:
        foreach($tree as $element){
            $childIds[] = $element['id'];
        }

        return $childIds;
    }

    /**
     * Return tree elements as appended childes.
     *
     * @param $data
     * @param $parent_id
     * @param bool $thisIsFirstRound
     * @return array
     */
    private function childes($data, $parent_id, $thisIsFirstRound = true){

        // if this is not root (parent is not null), we have to get
        // the parent of parent to handle it in tree.
        if($parent_id != null && $thisIsFirstRound){
            for($i=0; $i < count($data) - 1; $i++){
                if($data[$i]['id'] == $parent_id){
                    $parent_id = $data[$i]['parent_id'];
                    break;
                }
            }
        }

        $thisIsFirstRound = false;

        $return = [];
        foreach($data as $rowIndex=>$rowValue){
            $rowValue['parent_id'] = $rowValue['parent_id'] === '' ? null : $rowValue['parent_id'];
            if($rowValue['parent_id'] === $parent_id){
                $treeElement = $rowValue;
                $treeElement['_depth'] = substr_count($treeElement['path_of_sotring_in_tree'], '>');
                unset($treeElement['path_of_sotring_in_tree']);
                $treeElement['_childs'] = $this->childes($data, $treeElement['id'], $thisIsFirstRound);
                $return[$treeElement['id']] = (object) $treeElement;
            }
        }

        return $return;
    }

    /**
     * get reserved sorting under un item
     *
     * @param $parent_id
     * @return array
     */
    private function soringByParent($parent_id){
        $query = '
                    SELECT
                        sorting
                    FROM '. $this->table .'
                    WHERE parent_id = ' . $parent_id . '
                    ORDER BY sorting
                ';

        $fluentPdo = \Icore\Queryforge\DB::getPDO($this->connectionName);
        $stmt = $fluentPdo->query($query);
        $supTreeSorting = $stmt->fetchall(\PDO::FETCH_ASSOC);

        $return = [];
        if(!$supTreeSorting){
            return $return;
        }

        foreach($supTreeSorting as $sortingArray){
            $return[] = $sortingArray['sorting'];
        }

        return $return;
    }

    /**
     * Return the columns that will be selected in this model
     *
     * @return array
     */
    private function getColumnsName($prefix = ''){

        $columns = array_keys(static::$columns);

        if($prefix != ''){
            foreach($columns as $k=>$columnName){
                $columns[$k] = $prefix . '.' . $columnName;
            }
        }

        return $columns;
    }

    /**
     * Shift sorting in order to insert new item in reserved position.
     *
     * @param $parent
     * @param $chindSorting
     * @return mixed
     * @throws \Exception
     */
    private function shif($parent, $chindSorting){

        \Icore\Queryforge\DB::transStart();

        $sql = 'UPDATE ' . $this->table . ' SET sorting = sorting + 1 WHERE parent_id = ? and sorting >= ?';
        $fluentPdo = \Icore\Queryforge\DB::getPDO($this->connectionName);

        $return = true;
        try {
            $return = $fluentPdo->prepare($sql)->execute([$parent, $chindSorting]);
        } catch (\Exception $e){
            \Icore\Queryforge\DB::rollBack();
            $this->errors = $e->getMessage();
            return false;
        }

        \Icore\Queryforge\DB::commit();
        return $return;
    }

    /**
     * if there is spaces between sorting orders, so this method will remove the spaces
     * for example, if the sorting is (1,2,8,10), the soring will be updated to (1,2,3,4)
     *
     * @param $parent_id
     * @return bool
     * @throws \Exception
     */
    private function arrangeSortingSequence($parent_id){
        $query = $this->buildQuery($parent_id);
        $tree = $this->runQuery($query);

        $childesSorting = [];
        foreach($tree as $k=>$data){
            if($data['id'] == $parent_id){
                continue;
            }

            $childesSorting[$data['id']] = $k;
        }

        try {
            \Icore\Queryforge\DB::transStart();

            foreach($childesSorting as $id => $newSorting){
                $sql = 'UPDATE ' . $this->table . ' SET sorting = ' . $newSorting . ' WHERE id=' . $id;
                $fluentPdo = \Icore\Queryforge\DB::getPDO($this->connectionName);
                $fluentPdo->prepare($sql)->execute();
            }

            \Icore\Queryforge\DB::commit();
        } catch (\Exception $e){
            \Icore\Queryforge\DB::rollBack();
            throw new \Exception($e->getMessage());
        }

        return true;
    }

    /**
     * @return mixed|void
     * @throws \Exception
     */
    public function insert(){
        throw new \Exception('The "insert()" is deprecated in TreeModel, use "add()" to insert new item.');
    }


    /**
     * @param null $id
     * @return mixed|void
     * @throws \Exception
     */
    public function update($id = NULL){
        throw new \Exception('The "update()" is deprecated in TreeModel, use "set()" to update an item.');
    }

    /**
     * @param null $ids
     * @return TreeModel|void
     * @throws \Exception
     */
    public function list($ids = NULL){
        throw new \Exception('The "list()" is deprecated in TreeModel, you can use "getTree" to fetch a list of items.');
    }
}
