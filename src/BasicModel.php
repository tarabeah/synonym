<?PHP namespace Icore\Synonym;

/**
 * @author Ahmad Tarbeya
 */
abstract class BasicModel extends ModelAbstract\BasicModel
{
    /**
     * Save current set of data
     *
     * @throws \Exception
     */
    public function save($setForId = null)
    {
        // if there is no id, that mean that this is new entry,otherwise this should be update
        // for existing data.
        if ($this->id == null && $setForId == null) {
            return $this->insert();
        } else {
            return $this->update($setForId);
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id = null)
    {
        // Prepare the Select where:
        $this->BaseQuery
            ->from($this->table)
            ->select($this->clms());

        if($id == null) {
            $this->BaseQuery->limit(0 . ', ' . 1);
        } else {
            $this->BaseQuery->where($this->table . '.' . static::$primaryId, $id, '=');
        }

        try {
            $data = $this->exec();

            if ($data) {
                $this->data = $data[0];
            } else {
                // There is no data, so will return null.
                $this->data = array();
                return $this;
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage() . ' ' . __LINE__);
        }

        $this->id = $this->data[static::$primaryId];

        return $this;
    }

    /**
     * Insert new data to the model table.
     *
     * @return mixed
     * @throws \Exception
     */
    public function insert()
    {
        try {

            $this->BaseQuery->insertInto($this->table)->values($this->newData);
            $this->id = $this->exec();

            $this->data = $this->newData;
            $this->newData = [];

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage() . ' ' . __LINE__);
        }

        return $this->id;
    }

    /**
     * update new data to the model table.
     *
     * @param null $id
     * @return mixed
     * @throws \Exception
     */
    public function update($id = null)
    {
        if($id != null && $this->id != null) {
            throw new \Exception('This object ise reserved by another entity');
        }

        $id = is_null($id) ? $this->id : $id;
        try {
            $this->BaseQuery->update($this->table)->set($this->newData)->where(static::$primaryId, $id, '=');

            $this->data = $this->newData;
            $this->newData = [];

            $return = $this->exec();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage() . ' ' . __LINE__);
        }

        return $return;
    }

    /**
     * delete row data by the model .
     *
     * @param null $id
     * @return $this|null
     * @throws \Exception
     */
    public function delete($id = null)
    {
        // if there is requested id(s), so there is no need to delete this object
        if (($id != null && $id != $this->id) || is_array($id)) {
            $return = $this->directDeleteById($id);
            return $return;
        }

        // here there is not specified id, so we will delete this element in model form database.
        try {

            $this->BaseQuery->deleteFrom($this->table)->where(static::$primaryId, $this->id, '=');
            $result = $this->exec();

            if ($id == $this->id) {
                // The Data for this element is null to make it detectable when request any element.
                $this->data = null;
                $this->newData = null;

            }

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage() . ' ' . __LINE__);
        }

        return $this;
    }

    /**
     * Return data based on activation
     *
     * @param int $status
     * @return $this
     * @throws \Exception
     */
    public function active($status = 1)
    {

        if ($status != 0 && $status != 1) {
            throw new \Exception('The status should be 0 or 1 ' . '(' . __LINE__ . ')');
        }

        $this->query()->where($this->table . '.active', $status, '=');

        return $this;
    }

    /**
     * Return list of Model as collection
     * @param null $ids
     * @return $this
     * @throws \Exception
     */
    public function list($ids=null)
    {
        $this->{'isThisCollection'} = true;
        if($this->BaseQueryReplacement) {
            $this->BaseQuery = $this->BaseQueryReplacement;
        } else {
            $this->BaseQuery->from($this->table)->select($this->clms());
        }
        if(is_array($ids)){
            $this->BaseQuery->where($this->table . '.' . static::$primaryId, $ids);
        }

        try {
            $data = $this->exec();
            if ($data) {
                // The data for this model is the collection result
                $this->data = $this->collection($data)->result();
            } else {
                // There is no data, so will return null.
                $this->data = array();
                return $this;
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage() . ' ' . __LINE__);
        }


        $this->id = null;

        // the return is collection of models
        $return = $this->collection($data);

        // This case when we would like to return orm() with one-to-one relation
        if($this->waitingForOneToOneCall){
            $this->waitingForOneToOneCall = false;
            $queryString = $this->queryString();
            $return = $return->result()[0];
            $return->setQueryString($queryString);

        }

        return $return;
    }

    /**
     * @param $offset
     * @param $length
     * @return $this
     */
    public function limit($offset, $length)
    {
        $this->query()->limit($offset . ', ' . $length);
        return $this;
    }

    public function addWhere($key, $val){
        $this->query()->where($key, $val, '=');
    }

    /**
     * Return columns from model class
     * @return mixed
     */
    static function getColumns(){
        return static::$columns;
    }
}
