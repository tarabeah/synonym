<?PHP namespace Icore\Synonym\Util;

Class ModelCollection
{
    private $data;
    private $result;
    private $class;
    private $queryString;
    private $count = 0;
    private $ids = [];

    function __construct($data, $ModelClass)
    {
        $this->data = $data;
        $this->class = '\\' . $ModelClass;
    }

    // Get list ob entries as Models in collection
    function getCollection($connectionName){

        $this->queryString = \Icore\Queryforge\Query::getLastQuery();

        foreach($this->data as $data){

            $this->count++;
            $this->ids[] = $data[$this->class::getPrimaryId()];

            $objAttrs = array(
                'id' => $data[$this->class::getPrimaryId()],
                'data' => $data,
                'BaseQuery' => ''
            );

            $modelObj = new $this->class();
            $modelObj->setConnectionName($connectionName);
            $modelObj->setAttributes($objAttrs);
            $modelObj->initQuery();

            $this->result[] = $modelObj;
        }

        unset($this->data);

        return $this;
    }

    /**
     * Return the results
     * @return mixed
     */
    public function result(){
        return $this->result;
    }

    /**
     * return the count of elements
     *
     * @return mixed
     */
    public function count(){
        return $this->count;
    }

    public function queryString(){
        return $this->queryString;
    }
}
