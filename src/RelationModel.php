<?PHP namespace Icore\Synonym;

abstract class RelationModel extends ModelAbstract\BasicModel
{
    protected static $tables = [];
    protected static $relation;
    protected $relationTypes = [];
    protected $mainTable = '';

    /**
     * This method used to handle connections by name
     *
     * @param $connectionName
     */
    public function setConnectionName($connectionName)
    {
        $this->connectionName = $connectionName;
        $this->initQuery();
    }

    /**
     * Initialize Base query from QueryForge class.
     * The base query used to build target query, and it is common object that could be updated
     * and customize.
     */
    public function initQuery()
    {
        // Clear Fluent object by creating new Queryforge\Query
        $this->BaseQuery = new \Icore\Queryforge\Query($this->connectionName);

        // Prepare variable to handle the the sequence of customization for query before call get() or all().
        $this->CustomQueryBeforeExecuteToGet = new \Icore\Synonym\CustomQueryBeforeExecuteToGet();
    }

    /**
     * change mani table.
     *
     * @param string $mainTable
     * @return $this
     */
    public function main($mainTable = '')
    {
        if(!$mainTable){
            return $this;
        }

        static::$tables[] = static::$tables['main'];
        unset(static::$tables['main']);
        foreach (static::$tables as $k => $table) {
            if ($table == $mainTable) {
                unset(static::$tables[$k]);
                static::$tables['main'] = $table;
                break;
            }
        }

        return $this;
    }

    /**
     * Get data object from relation
     * @throws \Exception
     */
    function orm()
    {
        // Build $this->BaseQuery object
        $this->BuildRelationQuery();
        try {
            $dbRows = $this->exec();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        if(!$dbRows){
            return [];
        }

        $this->isThisCollection = true;
        $this->data = $this->relationArrayToModel($dbRows);
        return $this;
    }

    /**
     * Get data object from Aggregate
     * @throws \Exception
     */
    function aggregate($aggregate)
    {
        // Build $this->BaseQuery object
        $this->BuildRelationQuery($aggregate);

        try {
            $dbRows = $this->exec();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        // This case use to handle count request:
        if(count($dbRows) == 1 && array_key_exists(0, $dbRows) && array_key_exists('count', $dbRows[0])){
            return $dbRows[0]['count'];
        }
        return null;
    }


    /**
     * Build the $this->BaseQuery object.
     *
     * @throws \Exception
     */
    function BuildRelationQuery($selectColumns = null)
    {
        if(is_null($selectColumns)) {
            $selectColumns = $this->getSelectedColumns();
        }

        $innerJoinTablesWithColumns = $this->getInnerJoinTablesColumns();

        $query = $this->BaseQuery->from($this->getMainTable());

        foreach($innerJoinTablesWithColumns as $innerJoinRelation){
            $query->innerJoin($innerJoinRelation['table'])->on($innerJoinRelation[0], $innerJoinRelation[1], '=');
        }

        $query->select(null);
        call_user_func_array(array($this->BaseQuery, 'select'), $selectColumns);

        if ($this->queryCustom !== null) {
            $this->queryCustom->buildQeury($this->BaseQuery);
        }

        return $query;
    }

    /**
     * Get list of select column names that we would like to fetch from our query:
     *
     * @return array
     */
    function getSelectedColumns(){

        // Handle table columns
        $tables = static::$tables;

        // Prepare selected column (target columns)
        $selectColumns = [];

        // for each table, get the selected columns and add it to the return value ($selectColumns)
        foreach ($tables as $table) {

            if(!class_exists('model\\' . $this->connectionName . '\\' . $table)) {
                include_once __SITE_PATH . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . $this->connectionName . DIRECTORY_SEPARATOR . $table . '.php';
            }

            $class = $this->getClassName($table);
            $columns = array_keys($class::getColumns());
            foreach ($columns as $column) {
                $selectColumns[] = $table . '.' . $column . ' `' . $table . '.' . $column . '`';
            }
        }

        return $selectColumns;
    }

    /**
     * Get full class name with namespace
     * @param $modelName
     * @return string
     */
    private function getClassName($modelName)
    {
        return '\model\\' . $this->connectionName . '\\' . $modelName;
    }

    /**
     * Return the join tables with their relations
     *
     * @return array
     * @throws \Exception
     */
    function getInnerJoinTablesColumns(){
        $return = [];
        $relation = static::$relation;

        // Check if there is $relation input
        if (!$relation) {
            return $return;
        }

        $tablesAreAddedToJoin[] = $this->getMainTable();

        $length = count(static::$relation);

        for($count=0; $count<$length; $count++){
            $relationElement = $relation[$count];
            if(count($relationElement) == 2){
                // This is many to many relation
                $length += 2;
                $relation[] = $relationElement[0];
                $relation[] = $relationElement[1];
                continue;
            } else {
                // this is not "many-to-many" relation:
                $firstTableName = strstr(key($relationElement), '.', true);
                $secondTableName = strstr(array_values($relationElement)[0], '.', true);

                if(in_array($firstTableName, $tablesAreAddedToJoin)){
                    $return[] = ['table' => $secondTableName, key($relationElement), array_values($relationElement)[0]];
                    $tablesAreAddedToJoin[] = $secondTableName;
                } else if(in_array($secondTableName, $tablesAreAddedToJoin)){
                    $return[] = ['table' => $firstTableName, key($relationElement), array_values($relationElement)[0]];
                    $tablesAreAddedToJoin[] = $firstTableName;
                } else {
                    $relation[] = $relationElement;
                    $length++;
                }

                if($length == 100){
                    throw new \Exception('After more than 100 round still can\'t find inner join order fo tables');
                }
            }
        }

        return $return;
    }

    /**
     * Get main table from relation
     * @return mixed
     */
    private function getMainTable()
    {
        return static::$tables['main'];
    }

    /**
     * Convert array to chunk to models based on row name
     *
     * @param $dbRows
     * @return array
     */
    private function relationArrayToModel($dbRows){

        //  Categorise $dbRows based On model as chunk arrays:
        $categoriseBasedOnModel = [];
        foreach($dbRows as $k=>$dbRow){
            foreach($dbRow as $column=>$value){
                $tableName = strstr($column, '.', true);
                $column = trim(strstr($column, '.'), '.');
                $categoriseBasedOnModel[$k][$tableName][$column] = $value;
            }
        }

        // covert chunks to object models:
        $return = [];
        foreach($categoriseBasedOnModel as $k=>$modelData){
            $rowObjects = [];
            foreach($modelData as $modelName=>$modelData){

                $modelClassName = '\model\\' . $this->connectionName . '\\' . $modelName;

                if (!class_exists($modelClassName)) {
                    include_once __SITE_PATH . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . $this->connectionName . DIRECTORY_SEPARATOR . $modelName . '.php';
                }

                $modeObj = new $modelClassName();
                $modeObj->setConnectionName($this->connectionName);
                // $modeObj->setQueryString($this->queryString());

                $objAttrs = array(
                    'id' => $modelData[$modelClassName::getPrimaryId()],
                    'data' => $modelData,
                    'BaseQuery' => ''
                );

                $modeObj->setAttributes($objAttrs);
                $rowObjects[$modelName] = $modeObj;
            }

            $return[$k] = (object) $rowObjects;
        }

        return $return;
    }
}
