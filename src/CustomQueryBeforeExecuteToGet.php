<?PHP namespace Icore\Synonym;

Class CustomQueryBeforeExecuteToGet {

    private $instructions = [];
    private $buildQeuryInBefore = false;

    /**
     * @param $name
     * @param $arguments
     */
    function __call($name, $arguments)
    {
        $this->instructions[] = ['method' => $name, 'arguments' => $arguments];
    }

    /**
     * @param $BaseQuery
     */
    function buildQeury(& $BaseQuery){
        if($this->buildQeuryInBefore){
            return;
        }
        $this->buildQeuryInBefore = true;
        foreach($this->instructions as $queryPart){
            $method = $queryPart['method'];
            $args = $queryPart['arguments'];
            $BaseQuery = call_user_func_array(array($BaseQuery, $method), $args);
        }
    }
}
