<?PHP

Class user extends Icore\Synonym\BasicModel {

    static protected $columns = array(
        'id' => 'int',
        'country_id' => array('int' => 'country.id'),
        'type' => array('admin', 'author'),
        'name' => 'string',
    );

    static protected array $relations = [
        'country' => ['one-to-one' => ['country_id' => 'country.id']], // many-to-one
        'comment' => ['one-to-many' => ['id' => 'comment.user_id']],
        'books' => ['many-to-many' => ['books.id' => 'users_books.book_id', 'users_books.user_id' => 'user.id']]
    ];
}
