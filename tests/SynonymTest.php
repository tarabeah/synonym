<?php

use PHPUnit\Framework\TestCase;

include 'Related' . DIRECTORY_SEPARATOR . 'User.php';
include 'Related' . DIRECTORY_SEPARATOR . 'books.php';
include 'Related' . DIRECTORY_SEPARATOR . 'country.php';

/**
 *  Corresponding Class to test BasicModelTest class
 *
 * @author Atarbeya
 */
class BasicModelTest extends TestCase
{
    function setUp()
    {
        copy('tests/db/db_tmp.sqlite', 'tests/db/db.sqlite');
        $con = new \Icore\Queryforge\DB();
        $con->define('main', 'sqlite', 'tests/db/db.sqlite');
    }

    /**
     * return user model (without mock)
     *
     * @param null $id
     * @return user
     * @throws Exception
     */
    function getUserModel($id = null)
    {
        $users = new \user();
        $users->setConnectionName('main');

        if (!is_null($id)) {
            $users->get($id);
        }
        return $users;
    }

    /**
     * Check BasicModel->get(id)
     *
     * @throws Exception
     */
    public function testGet()
    {
        $userModel = $this->getUserModel()->get(1);
        $query = $userModel->queryString();
        $query = str_replace("\n", ' ', $query);

        // Check if the SQL query is correct
        $this->assertEquals($query, 'SELECT `id`, `country_id`, `type`, `name` FROM user WHERE  user.id = 1');

        // Check if retrieved data match to expected
        $this->assertEquals($userModel->result(), array(
            'id' => '1',
            'country_id' => '1',
            'type' => 'admin',
            'name' => 'Marek'
        ));

        // test get no data
        $userModel = $this->getUserModel()->get(100);
        $this->assertEquals($userModel->result(), array());
    }

    /**
     * Check BasicModel->insert()
     *
     * @throws Exception
     */
    public function testInsert()
    {
        $userModel = $this->getUserModel();
        $userModel->data(['type' => 'admin', 'name' => 'Ahmad', 'country_id' => 1]);
        $userModel->insert();

        $this->assertEquals($userModel->result(), array(
            'id' => '6',
            'country_id' => '1',
            'type' => 'admin',
            'name' => 'Ahmad'
        ));
    }

    /**
     * Check BasicModel->insert()
     *
     * @throws Exception
     */
    public function testDelete()
    {
        // Insert new row in order to test it's delete:
        $userModel = $this->getUserModel();
        $userModel->data(['type' => 'admin', 'name' => 'Ahmad', 'country_id' => 1]);
        $userModel->insert();

        // Delete By Id directly
        $userModel = $this->getUserModel();
        $deleteResult = $userModel->delete(6);
        $this->assertEquals($deleteResult, 1);


        // insert new row
        $userModel = $this->getUserModel();
        $userModel->data(['type' => 'admin', 'name' => 'Ahmad', 'country_id' => 1]);
        $userModel->insert();

        // Delete it by model
        $userModel = $this->getUserModel();
        $userModel->get(7);
        $deleteResult = $userModel->delete();
        $this->assertEquals($deleteResult, 1);

        // Delete it by model
        $userModel = $this->getUserModel();
        $userModel->get(2);
        $deleteResult = $userModel->delete(2);

        $this->assertEquals($deleteResult, 1);
    }

    /**
     * Delete list of ids.
     *
     * @throws Exception
     */
    public function testDeleteList()
    {
        // Insert new row in order to test it's delete:
        $userModel = $this->getUserModel();
        $deleteResult = $userModel->delete([1, 2, 6]);
        $this->assertEquals($deleteResult, 2);
    }

    /**
     * Test update data:
     *
     * @throws Exception
     */
    public function testUpdate()
    {
        $userModel = $this->getUserModel();
        $userModel->data(['name' => 'Ahmad', 'country_id' => 1]);
        $userModel->update(1);

        $userModel = $this->getUserModel(1);

        $data = $userModel->result();
        $this->assertEquals($data['name'], 'Ahmad');
    }

    /**
     * Test update data:
     *
     * @throws Exception
     */
    public function testUpdate_onEntity()
    {
        $userModel = $this->getUserModel();
        $userModel->data('name', 'Ahmad');
        $userModel->update(1);

        $userModel = $this->getUserModel();
        $userModel->get(1);
        $data = $userModel->result();

        $this->assertEquals($data['name'], 'Ahmad');
    }


    /**
     * Test get collection of models:
     * @throws Exception
     */
    public function testList()
    {
        $userModel = $this->getUserModel();
        $collection = $userModel->list();

        $queryString = str_replace("\n", ' ', $collection->queryString());
        $this->assertEquals($queryString, 'SELECT `id`, `country_id`, `type`, `name` FROM user');
        $this->assertEquals($collection->count(), 2);

        $collection = $userModel->active(0)->list();

        $this->assertEquals($collection->result(), array());
    }

    /**
     * Test delete by using invalid id (not digit)
     *
     * @throws Exception
     */
    public function testDelete_invalidParams()
    {
        $this->expectException(Exception::class);
        $userModel = $this->getUserModel();
        $userModel->delete('id_text');
    }

    /**
     * Test update by using invalid column name
     *
     * @throws Exception
     */
    public function testUpdate_invalidParams()
    {
        $this->expectException(Exception::class);

        $userModel = $this->getUserModel();
        $userModel->data('not-exist', 'Ahmad');
        $userModel->update(1);
    }

    /**
     * Test using customer query with active status
     *
     * @throws Exception
     */
    public function testActive_WithCustomQuery()
    {
        $userModel = $this->getUserModel();
        $userModel->active(1)->get(1);
        $result = $userModel->result();

        $this->assertEquals($userModel->result(), array(
            'id' => '1',
            'country_id' => '1',
            'type' => 'admin',
            'name' => 'Marek'
        ));
    }

    /**
     * Test save (it could be insert or update)
     */
    function testSave(){
        $userModel = $this->getUserModel();
        $userModel->data(['type' => 'admin', 'name' => 'Ahmad', 'country_id' => 1]);
        $userModel->save();

        $userModel->data(['type' => 'admin', 'name' => 'Ahmad Tarbeya', 'country_id' => 1]);
        $userModel->save();

        $this->assertEquals($userModel->result(), array(
            'id' => 6,
            'type' => 'admin',
            'name' => 'Ahmad Tarbeya',
            'country_id' => 1
        ));
    }

    /**
     * Test if the Get() has exception by its query
     */
    function testGet_withException(){

        $this->expectException(Exception::class);

        $httpClientMock = $this->getMockBuilder(\user::class)
            ->setMethods(['exec'])
            ->getMock();
        $httpClientMock->expects($this->once())
            ->method('exec')
            ->will($this->throwException(new Exception()));

        $httpClientMock->setConnectionName('main');
        $httpClientMock->get(7);
    }

    /**
     * Test if the insert() has exception by its query
     */
    function testInsert_withException(){

        $this->expectException(Exception::class);

        $httpClientMock = $this->getMockBuilder(\user::class)
            ->setMethods(['exec'])
            ->getMock();
        $httpClientMock->expects($this->once())
            ->method('exec')
            ->will($this->throwException(new Exception()));


        $httpClientMock->setConnectionName('main');
        $httpClientMock->data(['type' => 'admin', 'name' => 'Ahmad', 'country_id' => 1]);
        $httpClientMock->insert();
    }

    /**
     * Test if the list() has exception by its query
     */
    function testList_withException(){

        $this->expectException(Exception::class);

        $httpClientMock = $this->getMockBuilder(\user::class)
            ->setMethods(['exec'])
            ->getMock();
        $httpClientMock->expects($this->once())
            ->method('exec')
            ->will($this->throwException(new Exception()));


        $httpClientMock->setConnectionName('main');
        $httpClientMock->list();
    }

    /**
     * Test if the delete() has exception by its query
     */
    function testDelete_withException(){

        $this->expectException(Exception::class);

        $httpClientMock = $this->getMockBuilder(\user::class)
            ->setMethods(['exec'])
            ->getMock();

        $httpClientMock->setConnectionName('main');
        $httpClientMock->get(1);

        $httpClientMock->expects($this->any())
            ->method('exec')
            ->will($this->throwException(new Exception()));

        $httpClientMock->delete();
    }

    /**
     * Test if the update() has exception by its query
     */
    function testUpdate_withException(){

        $this->expectException(Exception::class);

        $httpClientMock = $this->getMockBuilder(\user::class)
            ->setMethods(['exec'])
            ->getMock();

        $httpClientMock->setConnectionName('main');
        $httpClientMock->get(1);

        $httpClientMock->expects($this->any())
            ->method('exec')
            ->will($this->throwException(new Exception()));


        $httpClientMock->data('name', 'Ahmad');
        $httpClientMock->update(1);
    }

    /**
     * Check BasicModel->Active() with Exception
     *
     * @throws Exception
     */
    public function testActive()
    {
        $this->expectException(Exception::class);
        $userModel = $this->getUserModel()->active(3)->get(1);
    }

    public function testOneToOneRelation(){
        $userModel = $this->getUserModel(1);
        $userCountry = $userModel->bind('country')->orm();
        $result = $userCountry->result();
        $this->assertEquals($result['id'], 1);
        $this->assertEquals($result['name'], 'Slovakia');
    }

    public function testOneToManyRelation(){
        $userModel = $this->getUserModel(1);
        $userCountry = $userModel->bind('books')->limit(0,1)->orm();
        $result = $userCountry->result();

        $data = $result[0]->result();
        $this->assertEquals($data['id'], 1);
        $this->assertEquals($data['book_name'], 'PHP master');

    }
}
